def archiver(string: str) -> str:
    counter = 0
    res = []
    let = ''
    for i in s:
        if i == let:
            counter += 1
        else:
            res.append(let)
            if counter > 1:
                res.append(str(counter))
            let = i
            counter = 1
    res.append(let)
    if counter > 1:
        res.append(str(counter))
    return "".join(res)

if __name__ == "__main__":
    assert archiver("AAABBBBCCC") == "A3B4C3"
    assert archiver("AAAAAA") == "A6"
    assert archiver("ABBBBCDE") == "AB4CDE"
    assert archiver("AABBBA") == "A2B3A"
