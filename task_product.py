from typing import List


def product_except_self(nums: List[int]) -> List[int]:
    counter = 0
    num = 1
    answer = []
    while counter < len(nums):
        for i in range(0, len(nums)):
            if i == counter:
                num == num
            else:
                num *= nums[i]
        answer.append(num)
        num = 1
        counter += 1
    return answer 


if __name__ == "__main__":
    assert product_except_self([1, 2, 3]) == [6, 3, 2]
    assert product_except_self([1, 0, 3]) == [0, 3, 0]
    assert product_except_self([0, 0, 3]) == [0, 0, 0]
