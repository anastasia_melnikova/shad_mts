def same_strings(string1: str, string2: str) -> bool:
    list1 = []
    list2 = []
    for i in s1:
        list1.append(i)
    list1.sort()
    for j in s2:
        list2.append(j)
    list2.sort()
    return list1 == list2


if __name__ == "__main__":
    assert same_strings("foo", "oof")
    assert not same_strings("foo", "bar")
    assert not same_strings("foo", "ofoo")
