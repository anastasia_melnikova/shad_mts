from typing import List


def max_profit(prices: List[int]) -> int:
    pos = 0
    diff = 0
    for i in range(0, len(prices)):
        if prices[pos] > prices[i]:
            pos = i
        if prices[i] - prices[pos] > diff:
            diff = prices[i] - prices[pos]
    return diff


if __name__ == "__main__":
    assert max_profit([7, 1, 5, 3, 6, 4]) == 5
    assert max_profit([7, 6, 4, 3, 1]) == 0
